/*
 * NMEA.c
 *
 *      Author: embedownik
 */

#include <string.h>
#include <stdlib.h>

#include <NMEA.h>
#include "NMEAChecksum.h"

#if ENABLE_PRINT_FUNCTION == 1

void GPS_print_GPGGA_data(GPS_GPGGA_data_s* GPS_GPGGA_data)
{
    printf("\r\n");
    printf("GPGGA struct data:\r\n");
    printf("hours:     %d\r\n", GPS_GPGGA_data->hours);
    printf("minutes:   %d\r\n", GPS_GPGGA_data->minutes);
    printf("seconds:   %d\r\n", GPS_GPGGA_data->sec);
    printf("msecs:     %d\r\n", GPS_GPGGA_data->msec);
    printf("fix:       %d\r\n", (uint8_t)GPS_GPGGA_data->fixQuality);
    printf("satellites:%d\r\n", GPS_GPGGA_data->satellites);
    printf("\r\n");
}

void GPS_print_GPRMC_data(GPS_GPRMC_data_s* GPS_GPRMC_data)
{
    printf("\r\n");
    printf("GPRMC struct data:\r\n");
    printf("hours:    %d\r\n", GPS_GPRMC_data->hours);
    printf("minutes:  %d\r\n", GPS_GPRMC_data->minutes);
    printf("seconds:  %d\r\n", GPS_GPRMC_data->sec);
    printf("msecs:    %d\r\n", GPS_GPRMC_data->msec);
    printf("day:      %d\r\n", GPS_GPRMC_data->day);
    printf("month:    %d\r\n", GPS_GPRMC_data->month);
    printf("year:     %d\r\n", GPS_GPRMC_data->year);
    printf("\r\n");
}

#endif

/**
 * Static function to find a selected letter position in C-string.
 */
static uint8_t GPS_find_letter(const char* wsk, char chr, uint8_t count)
{
    uint8_t position = 0;
    uint8_t cnt      = 0;

    for(uint8_t i = 0; wsk[i] != 0; i++)
    {
        if(wsk[i] == chr)
        {
            cnt++;

            if(cnt == count)
            {
                position = i;
                break;
            }
        }
    }

    return position;
}

/**
 * Simple convert function.
 */
static uint8_t GPS_2char2uint(const char* wsk)
{
    uint8_t value;

    value = (wsk[0] - '0') * 10U + (wsk[1] - '0');

    return value;
}

/**
 * Simple convert function.
 */
static uint16_t GPS_3char2uint16(const char* wsk)
{
    uint16_t value;

    value = (wsk[0] - '0') * 100U + (wsk[1] - '0') * 10U + (wsk[2] - '0');

    return value;
}

/**
 *  Parsing GPGGA frame.
 *
 *  @return parse status. 0 - success, 1 - error.
 */
bool NMEA_Parse_GPGGA(const char* ptr, GPS_GPGGA_data_s* GPS_GPGGA_data)
{
    bool status = false;

    if(strncmp(ptr, "$GPGGA,", 7U) == 0U)
    {
        if(NMEAChecksum_verify((uint8_t*)ptr))
        {
            //! time always on the same positions
            GPS_GPGGA_data->time.hours   = GPS_2char2uint(ptr + 7U);
            GPS_GPGGA_data->time.minutes = GPS_2char2uint(ptr + 9U);
            GPS_GPGGA_data->time.sec     = GPS_2char2uint(ptr + 11U);
            GPS_GPGGA_data->time.msec    = GPS_3char2uint16(ptr + 14U);

            //! search for 6th "," position
            uint8_t current_pos = GPS_find_letter(ptr, ',', 6U);

            GPS_GPGGA_data->fixQuality = (GPGGA_fixQuality_e)(ptr[current_pos + 1U] - '0');

            //! decode satellites data - possibles formats -> "0x" or "xx"
            GPS_GPGGA_data->satellites = atoi((ptr + current_pos + 3U));

            status = true;
        }
    }

    return status;
}

/**
 *  Parsing GPRMC frame.
 *
 *  @return parse status. 0 - success, 1 - error.
 */
bool NMEA_Parse_GPRMC(const char* ptr, GPS_GPRMC_data_s* GPS_GPRMC_data)
{
    bool status = false;

    if(strncmp(ptr, "$GPRMC,", 7U) == 0)
    {
        if(NMEAChecksum_verify((uint8_t*)ptr))
        {
            //! time always on the same positions
            GPS_GPRMC_data->positionTime.hours   = GPS_2char2uint(ptr + 7U);
            GPS_GPRMC_data->positionTime.minutes = GPS_2char2uint(ptr + 9U);
            GPS_GPRMC_data->positionTime.sec     = GPS_2char2uint(ptr + 11U);
            GPS_GPRMC_data->positionTime.msec    = GPS_3char2uint16(ptr + 14U);

            //! date from constant position
            uint8_t date_pos = GPS_find_letter(ptr, ',', 9U);

            //! date decode
            GPS_GPRMC_data->positionDate.day   = GPS_2char2uint(ptr + date_pos + 1U);
            GPS_GPRMC_data->positionDate.month = GPS_2char2uint(ptr + date_pos + 3U);
            GPS_GPRMC_data->positionDate.year  = GPS_2char2uint(ptr + date_pos + 5U);

            status = true;
        }
    }

    return status;
}
