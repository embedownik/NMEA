#include "NMEAChecksum.h"

static uint8_t parseChecksum(const uint8_t* buff)
{
    uint8_t checksum = 0U;

    if(buff[0U] <= '9')
    {
        checksum += buff[0U] - '0';
    }
    else
    {
        checksum += (buff[0U] - 'A') + 10U;
    }

    checksum = checksum * 16U;

    if(buff[1] <= '9')
    {
        checksum += buff[1U] - '0';
    }
    else
    {
        checksum += (buff[1U] - 'A') + 10U;
    }

    return checksum;
}

static uint8_t calc_NMEAChecksum(const uint8_t* buf)
{
    uint8_t sign;
    uint8_t checksum = 0U;

    for(uint8_t i = 0U; i < NMEA_MAX_MESSAGE_LEN; i++)
    {
        sign = buf[i];

        switch(sign)
        {
            case '$':
            {
                break;
            }
            case '*':
            {
                i = NMEA_MAX_MESSAGE_LEN;

                continue;
            }
            default:
            {
                if(checksum == 0U)
                {
                    checksum = sign;
                }
                else
                {
                    checksum = checksum ^ sign;
                }

                break;
            }
        }
    }

    return checksum;
}

bool NMEAChecksum_verify(const uint8_t* buff)
{
    uint8_t asteriskPosition = 0U;

    bool    checksumCorrect = false;

    for(uint8_t i = 0U; i < NMEA_MAX_MESSAGE_LEN; i++)
    {
        if(buff[i] == '*')
        {
            asteriskPosition = i;
            break; /* without this - tests on PC - no problem, problem on device */
        }
    }

    if(asteriskPosition != 0U)
    {
        uint8_t checksumPosition = asteriskPosition + 1U;

        uint8_t parsedChecksum = parseChecksum(&(buff[checksumPosition]));

        uint8_t calculatedChecksum = calc_NMEAChecksum(buff);

        if(calculatedChecksum == parsedChecksum)
        {
            checksumCorrect = true;
        }
    }

    return checksumCorrect;
}
