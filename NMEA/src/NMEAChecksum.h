/*
 *  NMEAChecksum.h
 *
 *  Separate module for Checksum calculation/verification.
 *
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

#include <NMEA.h>

bool NMEAChecksum_verify(const uint8_t* buff);

#ifdef __cplusplus
}
#endif
