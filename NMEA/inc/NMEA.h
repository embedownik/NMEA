/*
 * NMEA.h
 *
 *      Author: embedownik
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

/* Max message length from standard documentation */
#define NMEA_MAX_MESSAGE_LEN (82U)

typedef enum
{
    GPGGA_fix_quality_invalid  = 0,
    GPGGA_fix_quality_GPS_fix  = 1,
    GPGGA_fix_quality_DGPS_fix = 2,
} GPGGA_fixQuality_e;

typedef struct
{
    uint8_t            hours;
    uint8_t            minutes;
    uint8_t            sec;
    uint16_t           msec;
} GPS_time_s;

typedef struct
{
    uint8_t  day;
    uint8_t  month;
    uint8_t  year;
} GPS_date_s;

//! Struct with basic data from GPGGA - in current project I don't need a position (only time)
//! more parameters TODO
typedef struct
{
    GPS_time_s         time;

    GPGGA_fixQuality_e fixQuality;

    uint8_t            satellites;
} GPS_GPGGA_data_s;

//! Struct with basic data from GPGGA - in current project I don't need a position (only time)
//! more parameters TODO
typedef struct
{
    GPS_time_s positionTime;

    GPS_date_s positionDate;
} GPS_GPRMC_data_s;

/**
 *  Parsing GPGGA frame.
 *
 *  @return parse status. true - success, false - error.
 */
bool NMEA_Parse_GPGGA(const char* ptr, GPS_GPGGA_data_s* GPS_GPGGA_data);

/**
 *  Parsing GPRMC frame.
 *
 *  @return parse status. true - success, false - error.
 */
bool NMEA_Parse_GPRMC(const char* ptr, GPS_GPRMC_data_s* GPS_GPRMC_data);

#if ENABLE_PRINT_FUNCTION == 1

void GPS_print_GPGGA_data(GPS_GPGGA_data_s* GPS_GPGGA_data);

void GPS_print_GPRMC_data(GPS_GPRMC_data_s* GPS_GPRMC_data);

#endif

#ifdef __cplusplus
}
#endif
