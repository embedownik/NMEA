#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include <stdio.h>

#include <NMEA.h>

#include "string.h"
using std::string;

char example_GPGGA_ok_1[]     = "$GPGGA,103655.000,5222.7815,N,01649.8520,E,1,5,1.30,69.3,M,40.9,M,,*67";
char example_GPGGA_wrongCRC[] = "$GPGGA,103655.000,5222.7815,N,01649.8520,E,1,5,1.30,69.3,M,40.9,M,,*68";
char example_GPGGA_err_1[]    = "$GPGGB,";

char example_GPGGA_ok_2[] = "$GPGGA,000108.799,,,,,0,0,,,M,,M,,*46";

char example_GPRMC_ok_1[]     = "$GPRMC,104754.000,A,5222.7708,N,01649.8635,E,0.46,310.70,180619,,,A*60";
char example_GPRMC_wrongCRC[] = "$GPRMC,104754.000,A,5222.7708,N,01649.8635,E,0.46,310.70,180619,,,A*61";
char example_GPRMC_err_1[]    = "$GPXRMC,";

/* ugly way of comparing - because structs are defined in c files */
bool compareTime(GPS_time_s* lhs, GPS_time_s* rhs)
{
    bool equal = false;

    if(lhs->hours == rhs->hours)
    {
        if(lhs->minutes == rhs->minutes)
        {
            if(lhs->sec == rhs->sec)
            {
                if(lhs->msec == rhs->msec)
                {
                    equal = true;
                }
            }
        }
    }

    return equal;
}

bool compareDate(GPS_date_s* lhs, GPS_date_s* rhs)
{
    bool equal = false;

    if(lhs->day == rhs->day)
    {
        if(lhs->month == rhs->month)
        {
            if(lhs->year == rhs->year)
            {
                equal = true;
            }
        }
    }

    return equal;
}

bool compare_GPGGA(GPS_GPGGA_data_s* lhs, GPS_GPGGA_data_s* rhs)
{
    bool equal = false;

    if(compareTime(&lhs->time, &rhs->time))
    {
        if(lhs->fixQuality == rhs->fixQuality)
        {
            if(lhs->satellites == rhs->satellites)
            {
                equal = true;
            }
        }
    }

    return equal;
}

bool compare_GPRMC(GPS_GPRMC_data_s* lhs, GPS_GPRMC_data_s* rhs)
{
    bool equal = false;

    if(compareTime(&lhs->positionTime, &rhs->positionTime))
    {
        if(compareDate(&lhs->positionDate, &rhs->positionDate))
        {
            equal = true;
        }
    }

    return equal;
}

TEST_CASE("GPGGA frames")
{
    SUBCASE("Correct GPGGA frame test1")
    {
        // GIVEN
        GPS_GPGGA_data_s expected_result_data = {
            .time = {
                .hours      = 10,
                .minutes    = 36,
                .sec        = 55,
                .msec       = 0,
            },
            .fixQuality = GPGGA_fix_quality_GPS_fix,
            .satellites = 5,
        };

        GPS_GPGGA_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPGGA(example_GPGGA_ok_1, &parsed_data);

        // THEN
        CHECK(result == true);
        CHECK(compare_GPGGA(&expected_result_data, &parsed_data) == true);
    }

    SUBCASE("Wrong GPGGA CRC test1")
    {
        // GIVEN
        GPS_GPGGA_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPGGA(example_GPGGA_wrongCRC, &parsed_data);

        // THEN
        CHECK(result == false); /* only check return value */
    }

    SUBCASE("Wrong GPGGA frame test1")
    {
        // GIVEN
        GPS_GPGGA_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPGGA(example_GPGGA_err_1, &parsed_data);

        // THEN
        CHECK(result == false); /* only check return value */
    }

    SUBCASE("Correct GPGGA frame test2")
    {
        // GIVEN
        GPS_GPGGA_data_s expected_result_data = {
             .time = {
                .hours      = 0,
                .minutes    = 1,
                .sec        = 8,
                .msec       = 799
             },
            .fixQuality = GPGGA_fix_quality_invalid,
            .satellites = 0,
        };

        GPS_GPGGA_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPGGA(example_GPGGA_ok_2, &parsed_data);

        // THEN
        CHECK(result == true);
        CHECK(compare_GPGGA(&expected_result_data, &parsed_data) == true);
    }
}

TEST_CASE("GPRMC frames")
{
    SUBCASE("Correct GPRMC frame test1")
    {
        // GIVEN
        GPS_GPRMC_data_s expected_result_data = {
             .positionTime = {
                .hours   = 10,
                .minutes = 47,
                .sec     = 54,
                .msec    = 0,
             },

             .positionDate = {
                .day   = 18,
                .month = 6,
                .year  = 19,
             },
        };

        GPS_GPRMC_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPRMC(example_GPRMC_ok_1, &parsed_data);

        // THEN
        CHECK(result == true);
        CHECK(compare_GPRMC(&expected_result_data, &parsed_data) == true);
    }

    SUBCASE("Wrong GPRMC CRC test1")
    {
        // GIVEN
        GPS_GPRMC_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPRMC(example_GPRMC_err_1, &parsed_data);

        // THEN
        CHECK(result == false);
    }

    SUBCASE("Wrong GPRMC frame test1")
    {
        // GIVEN
        GPS_GPRMC_data_s parsed_data = {0};

        // WHEN
        bool result = NMEA_Parse_GPRMC(example_GPRMC_err_1, &parsed_data);

        // THEN
        CHECK(result == false);
    }
}
