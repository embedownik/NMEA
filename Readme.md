Simple library to parse data in NMEA format from GPS receivers.

NMEA - National Marine Electronics Association.

General documentation: http://aprs.gids.nl/nmea/#gga

With a couple of Unit tests based on Doctest Framework - just run simple make and run test application.

Module required board.h file with settings like:

  - #define ENABLE_PRINT_FUNCTION 1
